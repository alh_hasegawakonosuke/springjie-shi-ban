<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザー編集画面</title>
	</head>
	<body>
	<a href="<%= request.getContextPath() %>/">ホーム画面へ</a>
	<a href="<%= request.getContextPath() %>/userManegement">戻る</a>

		<h1>${message}</h1>

		<form:form modelAttribute="editForm">
		<div><form:errors path="*"  /></div>
		<ul>
			<li>
				ログインID
				<form:input path="login_id" value="${editUser.login_id}"/>
			</li>
			<li>
				パスワード
				<form:input path="password" value="${editUser.password}"/>
			</li>
			<li>
				名前
				<form:input path="name" value="${editUser.name}"/>
			</li>
			<li>
				支店
				<form:select path="branch_id">
				<form:options items="${branch}" itemLabel="name" itemValue="id" />
				</form:select>
			</li>
			<li>
				役職
				<form:select path="department_id">
				<form:options items="${department}" itemLabel="name" itemValue="id" />
				</form:select>
			</li>
			<li>
				<input type="submit">
			</li>
		</ul>
		</form:form>

	</body>
</html>