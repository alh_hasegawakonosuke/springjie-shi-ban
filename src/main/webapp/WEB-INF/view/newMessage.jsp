<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿画面</title>
</head>
<body>
	<div class="header">
		<a href="<%= request.getContextPath() %>/">ホーム画面へ</a>
	</div>

	<h1>${message}</h1>

	<form:form modelAttribute="messageForm">
	<div><form:errors path="*"  /></div>
	<input type="hidden" name="user_id" value="${loginUser.id}">
	<ul>

		<li>
			件名
			<form:input path="subject" />
		</li>
		<li>
			本文
			<form:textarea path="text" />
		</li>
		<li>
			カテゴリー
			<form:input path="category" />
		</li>
		<li>
		<input type="submit">
		</li>
	</ul>

	</form:form>


</body>
</html>