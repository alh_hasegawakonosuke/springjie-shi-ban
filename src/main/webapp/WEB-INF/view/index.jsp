<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<html>

	<head>
		<link href="<c:url value="/resources/css/test.css" />" rel="stylesheet">
		<script src="//code.jquery.com/jquery-3.2.1.min.js" ></script>
		<script src="<c:url value="/resources/js/test.js" />"></script>
		<meta charset="utf-8">
		<title>home</title>

	<script type="text/javascript">
	<!--

	function check(){

		if(window.confirm("削除してよろしいですか")){
			return ture;
		}else{
			window.alert("キャンセルしました");
			return false;
		}
	}


    // -->
	</script>

	</head>
	<body>
	<div>
      <p id="myid">ここをクリックしてください。</p>
    </div>


		<!-- ヘッダー -->
		<div class="header">
			<a href="<%= request.getContextPath() %>/logout">ログアウト</a>
			<a href="<%= request.getContextPath() %>/signUp">新規登録画面へ</a>
			<a href="<%= request.getContextPath() %>/newMessage">新規投稿画面</a>
			<a href="<%= request.getContextPath() %>/userManegement">ユーザー管理画面</a>
		</div>

		<h1>${message}</h1>
		<c:out value="${loginUser.name}"></c:out>

		<!-- 投稿表示 -->
		<c:if test="${not empty messages}">
		<c:forEach items="${messages}" var="message">
		<ul>
			<li>
				投稿者：<c:out value="${message.name }"></c:out>
			</li>
			<li>
				件名：<c:out value="${message.subject}"></c:out>
			</li>
			<li>
				本文：<c:forEach var="text" items="${fn:split(message.text, '
										')}">
								<div>${text}</div>
							</c:forEach>
			</li>
			<li>
				カテゴリー：<c:out value="${message.category}"></c:out>
			</li>
			<li>
				投稿日時：<fmt:formatDate value="${message.created_date}" pattern="yyyy/MM/dd HH:mm:ss" ></fmt:formatDate>
			</li>

			<!-- 投稿削除 -->
			<li>
				<c:if test="${loginUser.id == message.user_id}">
					<form:form action="messageDelete" modelAttribute="messageDeleteForm" onSubmit="return check()">
						<input type="hidden" name="id" value="${message.id}">
						<input type="submit" value="投稿削除" >
					</form:form>
				</c:if>
			</li>

			<!-- コメント表示 -->
			<li>
				<c:forEach items="${comments}" var="comment">
				<c:if test="${message.id == comment.message_id}">
					<ul>
						<li>
						<c:out value="${comment.user_name}"></c:out>
						さんのコメント:<br />
						<c:forEach var="s" items="${fn:split(comment.text, '
										')}">
										<c:out value="${s}"></c:out><br/>
									</c:forEach>
						<fmt:formatDate value="${comment.created_date}" pattern="yyyy/MM/dd HH:mm:ss" ></fmt:formatDate>

						<!-- コメント削除 -->
						<c:if test="${loginUser.id == comment.user_id}">
							<form:form action="commentDelete"  modelAttribute="commentDeleteForm" onSubmit="return check()">
								<input type="hidden" name="id" value="${comment.id}">
								<input type="submit" value="コメント削除">
							</form:form>
						</c:if>

						</li>
					</ul>
				</c:if>
				</c:forEach>
			</li>

			<!-- コメント投稿 -->
			<li>
				<form:form action="comment" modelAttribute="commentForm">
					<input type="hidden" name="message_id" value="${message.id }">
					<input type="hidden" name="user_id" value="${loginUser.id }">
					<form:textarea path="text" />
					<input type="submit">
				</form:form>
			</li>
		</ul>
		</c:forEach>
		</c:if>
	</body>
</html>
