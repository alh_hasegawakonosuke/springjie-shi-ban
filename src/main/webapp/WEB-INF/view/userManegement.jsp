<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ユーザー管理画面</title>

	<script type="text/javascript">
	<!--

	function stop(){

		if(window.confirm("停止してよろしいですか？")){
			return ture;
		}else{
			window.alert("キャンセルしました");
			return false;
		}
	}
	function awake(){

		if(window.confirm("稼働させてよろしいですか？")){
			return ture;
		}else{
			window.alert("キャンセルしました");
			return false;
		}
	}

	// -->
	</script>

	</head>
	<body>
		<div class="header">
			<a href="<%= request.getContextPath() %>/">ホーム画面へ</a>
		</div>
		<h1>${message}</h1>
		<c:out value="${loginUser.name}"></c:out>

		<c:forEach items="${users}" var="user">
		<ul>
			<li>
				名前：<c:out value="${user.name}"></c:out>
			</li>
			<li>
				ログインID：<c:out value="${user.login_id }"></c:out>
			</li>
			<li>
				支店：<c:out value="${user.branch_id }"></c:out>
			</li>
			<li>
				部署：<c:out value="${user.department_id }"></c:out>
			</li>
			<li>
				<c:if test="${user.is_working == 0 }">
				稼働状況：稼働中
				</c:if>

				<c:if test="${user.is_working == 1 }">
				稼働状況：停止中
				</c:if>
			</li>
			<li>
				<a href="${pageContext.request.contextPath}/edit/${user.id}/">編集</a>
			</li>
			<li>
			<c:if test="${user.is_working == 0 }">
				<form:form action="userManegement" modelAttribute="switchForm" onSubmit="return stop()">
				<input type="hidden" name="id" value="${user.id}">
				<input type="hidden" name="is_working" value="1">
				<input type="submit" value="停止">
				</form:form>
			</c:if>

			<c:if test="${user.is_working == 1 }">
				<form:form action="userManegement" modelAttribute="switchForm" onSubmit="return awake()">
				<input type="hidden" name="id" value="${user.id}">
				<input type="hidden" name="is_working" value="0">
				<input type="submit" value="稼働">
				</form:form>
			</c:if>

			</li>


		</ul>
		</c:forEach>
	</body>
</html>