<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログイン</title>
</head>
<body>
	<div class="header">

	</div>
	<h1>${message}</h1>

	<div><c:out value = "${loginUser.name}"></c:out></div>
	<div>
		<a>
			<c:out value="${coution}"></c:out>
		</a>
		<p>
			<c:out value="${errorMessage}"></c:out>
		</p>
	</div>



		<form:form modelAttribute="loginForm">
		<div><form:errors path="*"  /></div>
		<ul>
			<li>
				ログインID
				<form:input path="login_id" />
			</li>
			<li>
				パスワード
				<form:password path="password" />
			</li>
			<li>
				<input type="submit">
			</li>
		</ul>
		</form:form>
</body>
</html>