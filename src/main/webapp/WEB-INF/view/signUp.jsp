<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規ユーザー登録</title>
</head>
<body>
	<div class="header">
		<a href="<%= request.getContextPath() %>/">ホーム画面へ</a>
	</div>

	<h1>${message}</h1>
	<div>
		<a>
			<c:out value="${coution}"></c:out>
		</a>
		<p>
			<c:out value="${errorMessage}"></c:out>
		</p>
	</div>

		<form:form modelAttribute="signUpForm">
		<div><form:errors path="*"  /></div>
		<ul>
			<li>
				ログインID
				<form:input path="login_id" />
			</li>
			<li>
				パスワード
				<form:password path="password" />
			</li>
			<li>
				名前
				<form:input path="name" />
			</li>
			<li>
				支店
				<form:select path="branch_id">
				<form:options items="${branch}" itemLabel="name" itemValue="id" />
				</form:select>
			</li>
			<li>
				役職
				<form:select path="department_id">
				<form:options items="${department}" itemLabel="name" itemValue="id" />
				</form:select>
			</li>
			<li>
				<input type="submit">
			</li>
		</ul>
		<c:out value="${loginUser.name}" />
		</form:form>


</body>
</html>