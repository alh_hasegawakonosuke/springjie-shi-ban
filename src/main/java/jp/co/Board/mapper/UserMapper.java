package jp.co.Board.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import jp.co.Board.entity.UserEntity;
import jp.co.Board.form.LoginForm;
import jp.co.Board.form.SignUpForm;
import jp.co.Board.form.SwitchForm;
import jp.co.Board.form.UserEditForm;

@MapperScan
public interface UserMapper {
	/*ログイン*/
	UserEntity getUser(LoginForm loginForm);

	/*新規登録*/
	int register(SignUpForm signUpForm);

	/*ユーザー管理画面一覧取得*/
	List<UserEntity> getUsers();

	/*編集するユーザー取得*/
	UserEntity getEditUser(int id);

	/*ユーザー情報編集*/
	int edit(UserEditForm userEditForm);

	/*稼働・停止管理*/
	int change(SwitchForm switchForm);
}
