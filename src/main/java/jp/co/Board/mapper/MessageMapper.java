package jp.co.Board.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import jp.co.Board.dto.UserMessageDto;
import jp.co.Board.form.MessageDeleteForm;
import jp.co.Board.form.MessageForm;

@MapperScan
public interface MessageMapper {
	int post(MessageForm messageForm);
	List<UserMessageDto> getMessage();
	int delete(MessageDeleteForm deleteForm);

}
