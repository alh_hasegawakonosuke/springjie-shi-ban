package jp.co.Board.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import jp.co.Board.entity.CommentEntity;
import jp.co.Board.form.CommentDeleteForm;
import jp.co.Board.form.CommentForm;

@MapperScan
public interface CommentMapper {

	/*コメント投稿*/
	int post(CommentForm commentForm);

	/*コメント表示（取得）*/
	List<CommentEntity> getComment();

	/*コメント削除*/
	int delete(CommentDeleteForm deleteForm);

}
