package jp.co.Board.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import jp.co.Board.entity.BranchEntity;

@MapperScan
public interface BranchMapper {

	List<BranchEntity> getBranch();

}
