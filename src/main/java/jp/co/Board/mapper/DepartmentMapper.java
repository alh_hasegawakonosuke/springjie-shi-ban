package jp.co.Board.mapper;

import java.util.List;

import org.mybatis.spring.annotation.MapperScan;

import jp.co.Board.entity.DepartmentEntity;

@MapperScan
public interface DepartmentMapper {

	List<DepartmentEntity> getDepartment();

}
