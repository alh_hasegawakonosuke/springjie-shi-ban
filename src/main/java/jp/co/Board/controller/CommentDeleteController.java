package jp.co.Board.controller;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.Board.form.CommentDeleteForm;
import jp.co.Board.service.CommentDeleteService;

@Controller
public class CommentDeleteController {

	@Autowired
	private CommentDeleteService deleteService;

	@RequestMapping(value = "/commentDelete", method = RequestMethod.POST)
	public String delete(@ModelAttribute CommentDeleteForm deleteForm, Model model) {
		int count = deleteService.delete(deleteForm);
		Logger.getLogger(CommentDeleteController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		return "redirect:/";
	}


}
