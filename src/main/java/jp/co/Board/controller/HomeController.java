package jp.co.Board.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.Board.dto.CommentDto;
import jp.co.Board.dto.UserMessageDto;
import jp.co.Board.form.CommentForm;
import jp.co.Board.service.ShowCommentService;
import jp.co.Board.service.ShowMessageService;

@Controller
public class HomeController {

	@Autowired
	private ShowMessageService messageService;
	@Autowired
	private ShowCommentService commentService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model) {

		/*タイトル表示*/
		model.addAttribute("message", "HOME画面");

		/*メッセージ表示*/
		List<UserMessageDto> messages = messageService.getMessage();
		model.addAttribute("messages", messages);

		/*コメント投稿フォーム表示*/
		CommentForm commentForm = new CommentForm();
		model.addAttribute("commentForm", commentForm);

		/*コメント表示*/
		List<CommentDto> comments = commentService.getComment();
		model.addAttribute("comments", comments);

		return "/index";
	}



}
