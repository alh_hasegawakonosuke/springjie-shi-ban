package jp.co.Board.controller;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.Board.form.MessageForm;
import jp.co.Board.service.NewMessaeService;

@Controller
public class NewMessageController {

	@Autowired
	private NewMessaeService messageService;

	@RequestMapping(value = "/newMessage", method = RequestMethod.GET)
	public String post(Model model) {
		MessageForm messageForm = new MessageForm();
		model.addAttribute("messageForm", messageForm);
		model.addAttribute("message", "新規投稿画面");
		return "/newMessage";
	}

	@RequestMapping(value="/newMessage", method = RequestMethod.POST)
	public String postMessage(@Valid @ModelAttribute MessageForm messageForm, BindingResult result, Model model) {

		if(result.hasErrors()) {
			model.addAttribute("coution", "エラー");
			model.addAttribute("errorMessage", "以下のエラーを解消してください");
			model.addAttribute("message", "新規投稿画面");
			return "newMessage";
		}else {
			int count = messageService.post(messageForm);
			Logger.getLogger(NewMessageController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
			return "redirect:/";
		}
	}

}
