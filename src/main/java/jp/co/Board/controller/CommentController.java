package jp.co.Board.controller;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.Board.form.CommentForm;
import jp.co.Board.service.CommentService;

@Controller
public class CommentController {

	@Autowired
	private CommentService commentService;

	@RequestMapping(value = "/comment", method = RequestMethod.POST)
	public String home(@ModelAttribute CommentForm commentForm, Model model) {

		/*コメント投稿*/
		int count = commentService.post(commentForm);
		Logger.getLogger(CommentController.class).log(Level.INFO, "挿入件数は" + count + "件です。");

		return "redirect:/";
	}

}
