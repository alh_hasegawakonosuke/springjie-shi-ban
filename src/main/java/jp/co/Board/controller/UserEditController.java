package jp.co.Board.controller;

import java.util.List;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.Board.dto.BranchDto;
import jp.co.Board.dto.DepartmentDto;
import jp.co.Board.dto.UserDto;
import jp.co.Board.form.UserEditForm;
import jp.co.Board.service.BranchService;
import jp.co.Board.service.DepartmentService;
import jp.co.Board.service.UserEditService;

@Controller
public class UserEditController {

	@Autowired
	private UserEditService userEditService;
	@Autowired
	private BranchService branchService;
	@Autowired
	private DepartmentService departmentService;

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String getEditUser(Model model, @PathVariable int id) {
		UserDto editDto = userEditService.getUser(id);
		UserEditForm editForm = new UserEditForm();
		model.addAttribute("editUser", editDto);
		model.addAttribute("editForm", editForm);

		/*部署*/
		List<DepartmentDto> departmentList = departmentService.getDepartment();
		model.addAttribute("department", departmentList);

		/*支店*/
		List<BranchDto> branchList = branchService.getBranch();
		model.addAttribute("branch", branchList);

		BeanUtils.copyProperties(editDto, editForm);
		model.addAttribute("message", "ユーザー情報編集画面");
		return "edit";
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String edit(@ModelAttribute UserEditForm userEditForm, BindingResult result, Model model) {
		int count = userEditService.edit(userEditForm);
		Logger.getLogger(UserEditController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		return "redirect:/userManegement";

	}

}
