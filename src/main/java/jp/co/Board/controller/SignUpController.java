package jp.co.Board.controller;

import java.util.List;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.Board.dto.BranchDto;
import jp.co.Board.dto.DepartmentDto;
import jp.co.Board.form.SignUpForm;
import jp.co.Board.service.BranchService;
import jp.co.Board.service.DepartmentService;
import jp.co.Board.service.SignUpService;

@Controller
public class SignUpController {

	@Autowired
	private SignUpService signUpService;
	@Autowired
	private BranchService branchService;
	@Autowired
	private DepartmentService departmentService;

	@RequestMapping(value = "/signUp", method = RequestMethod.GET)
	public String signUp(Model model) {

		/*登録フォーム*/
		SignUpForm signUpForm = new SignUpForm();
		model.addAttribute("signUpForm", signUpForm);

		/*支店*/
		List<BranchDto> branchList = branchService.getBranch();
		model.addAttribute("branch", branchList);

		/*部署*/
		List<DepartmentDto> departmentList = departmentService.getDepartment();
		model.addAttribute("department", departmentList);

		model.addAttribute("message", "新規ユーザー登録");
		return "/signUp";
	}

	@RequestMapping(value = "/signUp", method = RequestMethod.POST)
	public String register(@Valid @ModelAttribute SignUpForm signUpForm, BindingResult result, Model model) {
		try {
			if(result.hasErrors()) {

				/*部署*/
				List<DepartmentDto> departmentList = departmentService.getDepartment();
				model.addAttribute("department", departmentList);

				/*支店*/
				List<BranchDto> branchList = branchService.getBranch();
				model.addAttribute("branch", branchList);

				model.addAttribute("coution", "エラー");
				model.addAttribute("errorMessage", "以下のエラーを解消してください");
				model.addAttribute("message", "新規ユーザー登録");
				return "signUp";
			}else {
				int count = signUpService.register(signUpForm);
			    Logger.getLogger(SignUpController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
			    return "redirect:/";
			}

		}catch(RuntimeException e) {
			model.addAttribute("message", "新規ユーザー登録");
			return "signUp";
		}


	}

}
