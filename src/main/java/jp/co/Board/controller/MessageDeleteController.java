package jp.co.Board.controller;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.Board.form.MessageDeleteForm;
import jp.co.Board.service.MessageDeleteService;

@Controller
public class MessageDeleteController {

	@Autowired
	private MessageDeleteService deleteService;

	@RequestMapping(value = "/messageDelete", method = RequestMethod.POST)
	public String delete(@ModelAttribute MessageDeleteForm deleteForm,Model model) {
		int count = deleteService.delete(deleteForm);
		Logger.getLogger(MessageDeleteController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		return "redirect:/";
	}

}
