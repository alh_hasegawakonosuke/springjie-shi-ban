package jp.co.Board.controller;

import java.util.List;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.Board.dto.UserDto;
import jp.co.Board.form.SwitchForm;
import jp.co.Board.service.SwitchService;
import jp.co.Board.service.UserManegementService;

@Controller
public class UserManegementController {

	@Autowired
	private UserManegementService userService;
	@Autowired
	private SwitchService switchService;

	@RequestMapping(value = "/userManegement", method = RequestMethod.GET)
	public String manegement(Model model) {
		/*タイトル表示*/
		model.addAttribute("message", "ユーザー管理画面");

		/*登録ユーザー一覧表示*/
		List<UserDto> users = userService.getUsers();
		model.addAttribute("users", users);

		return "/userManegement";
	}

	@RequestMapping(value = "/userManegement", method = RequestMethod.POST)
	public String manegement(@ModelAttribute SwitchForm switchForm, Model model) {
		/*	稼働・停止管理*/
		int count = switchService.change(switchForm);
		Logger.getLogger(UserEditController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		return "redirect:/userManegement";
	}

}
