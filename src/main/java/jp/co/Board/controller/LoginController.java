package jp.co.Board.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import jp.co.Board.dto.UserDto;
import jp.co.Board.form.LoginForm;
import jp.co.Board.service.LoginService;

@Controller
@SessionAttributes("loginUser")
public class LoginController {

	@Autowired
	private LoginService LoginService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginUser(Model model) {
		LoginForm loginForm = new LoginForm();
		model.addAttribute("loginForm", loginForm);
		model.addAttribute("message", "LOGIN画面");
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@Valid @ModelAttribute LoginForm loginForm, BindingResult result, Model model) {
		try {
			if(result.hasErrors()) {
				model.addAttribute("coution", "エラー");
				model.addAttribute("errorMessage", "以下のエラーを解消してください");
				model.addAttribute("message", "LOGIN画面");
				return "login";

			}else {
				UserDto loginUser = LoginService.getUser(loginForm);
				model.addAttribute("loginUser", loginUser);
				return "redirect:/";
			}
		}catch(IllegalArgumentException e) {
			model.addAttribute("message", "LOGIN画面");
			return "login";
		}

	}

//	@ModelAttribute("loginUser")
//	public  UserDto setUpLoginUser() {
//		return new UserDto();
//
//	}

}
