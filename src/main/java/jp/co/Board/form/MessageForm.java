package jp.co.Board.form;

import java.util.Date;

import org.hibernate.validator.constraints.NotEmpty;

public class MessageForm {

	private int id;
	@NotEmpty
	private String subject;
	@NotEmpty
	private String text;
	@NotEmpty
	private String category;
	private int user_id;
	private Date created_date;
	private Date updated_date;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}

	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public Date getcteated_date() {
		return created_date;
	}

	public void setcteated_date(Date cteated_date) {
		this.created_date = cteated_date;
	}
	public Date getUpdateDate() {
		return updated_date;
	}
	public void setUpdateDate(Date updateDate) {
		this.updated_date = updateDate;
	}




}
