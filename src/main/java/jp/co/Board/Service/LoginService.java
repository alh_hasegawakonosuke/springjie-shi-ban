package jp.co.Board.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.dto.UserDto;
import jp.co.Board.entity.UserEntity;
import jp.co.Board.form.LoginForm;
import jp.co.Board.mapper.UserMapper;

@Service
public class LoginService {

	@Autowired
	private UserMapper userMapper;

	public UserDto getUser(LoginForm loginForm) {
		UserDto dto = new UserDto();
		UserEntity entity = userMapper.getUser(loginForm);
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

}
