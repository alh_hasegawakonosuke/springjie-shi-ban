package jp.co.Board.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.form.MessageForm;
import jp.co.Board.mapper.MessageMapper;

@Service
public class NewMessaeService {

	@Autowired
	private MessageMapper messageMapper;

	public int post(MessageForm messageForm) {
		int count = messageMapper.post(messageForm);
		return count;
	}

}
