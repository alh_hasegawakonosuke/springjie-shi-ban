package jp.co.Board.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.dto.DepartmentDto;
import jp.co.Board.entity.DepartmentEntity;
import jp.co.Board.mapper.DepartmentMapper;

@Service
public class DepartmentService {

	@Autowired
	private DepartmentMapper departmentMapper;

	public List<DepartmentDto> getDepartment(){
		List<DepartmentEntity> branchList = departmentMapper.getDepartment();
		List<DepartmentDto> resultList = convertToDto(branchList);
		return resultList;

	}

	private List<DepartmentDto> convertToDto(List<DepartmentEntity> branchList){
		List<DepartmentDto> resultList = new LinkedList<>();
		for(DepartmentEntity entity : branchList) {
			DepartmentDto dto = new DepartmentDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}

		return resultList;
	}

}
