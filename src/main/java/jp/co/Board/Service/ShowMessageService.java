package jp.co.Board.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.dto.UserMessageDto;
import jp.co.Board.mapper.MessageMapper;

@Service
public class ShowMessageService {

	@Autowired
	private MessageMapper MessageMapper;

	public List<UserMessageDto> getMessage() {
		List<UserMessageDto> messageList = MessageMapper.getMessage();
		List<UserMessageDto> resultList = convertToDto(messageList);
		return resultList;
	}

	private List<UserMessageDto> convertToDto(List<UserMessageDto> messageList){
		List<UserMessageDto> resultList = new LinkedList<>();
		for(UserMessageDto dto : messageList) {

			resultList.add(dto);
		}
		return resultList;
	}

}
