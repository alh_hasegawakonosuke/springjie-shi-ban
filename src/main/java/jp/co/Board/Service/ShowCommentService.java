package jp.co.Board.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.dto.CommentDto;
import jp.co.Board.entity.CommentEntity;
import jp.co.Board.mapper.CommentMapper;

@Service
public class ShowCommentService {

	@Autowired
	private CommentMapper commentMapper;

	public List<CommentDto> getComment(){
		List<CommentEntity> commentList = commentMapper.getComment();
		List<CommentDto> resultList = convertToDto(commentList);
		return resultList;
	}

	private List<CommentDto> convertToDto(List<CommentEntity> commentList){
		List<CommentDto> resultList = new LinkedList<>();
		for(CommentEntity entity : commentList) {
			CommentDto dto = new CommentDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}

		return resultList;
	}

}
