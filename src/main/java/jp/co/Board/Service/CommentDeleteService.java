package jp.co.Board.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.form.CommentDeleteForm;
import jp.co.Board.mapper.CommentMapper;

@Service
public class CommentDeleteService {

	@Autowired
	private CommentMapper commentMapper;

	public int delete(CommentDeleteForm deleteForm) {
		int count = commentMapper.delete(deleteForm);
		return count;
	}

}
