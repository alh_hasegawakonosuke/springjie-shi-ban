package jp.co.Board.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.form.SwitchForm;
import jp.co.Board.mapper.UserMapper;

@Service
public class SwitchService {

	@Autowired
	private UserMapper userMapper;

	public int change(SwitchForm switchForm) {
		int count = userMapper.change(switchForm);
		return count;

	}

}
