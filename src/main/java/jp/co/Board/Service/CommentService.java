package jp.co.Board.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.form.CommentForm;
import jp.co.Board.mapper.CommentMapper;

@Service
public class CommentService {

	@Autowired
	private CommentMapper commentMapper;

	public int post(CommentForm commentForm) {
		int count = commentMapper.post(commentForm);
		return count;
	}

}
