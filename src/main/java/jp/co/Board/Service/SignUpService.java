package jp.co.Board.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.form.SignUpForm;
import jp.co.Board.mapper.UserMapper;

@Service
public class SignUpService {

	@Autowired
	private UserMapper userMapper;

	public int register(SignUpForm signUpForm) {
		int count = userMapper.register(signUpForm);
		return count;
	}

}
