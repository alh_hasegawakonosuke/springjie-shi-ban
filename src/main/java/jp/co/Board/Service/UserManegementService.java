package jp.co.Board.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.dto.UserDto;
import jp.co.Board.entity.UserEntity;
import jp.co.Board.mapper.UserMapper;

@Service
public class UserManegementService {

	@Autowired
	private UserMapper userMapper;

	public List<UserDto> getUsers(){
		List<UserEntity> userList = userMapper.getUsers();
		List<UserDto> resultList = convertToDto(userList);
		return resultList;
	}

	private List<UserDto> convertToDto(List<UserEntity> userList){
		List<UserDto> resultList = new LinkedList<>();
		for(UserEntity entity : userList) {
			UserDto dto = new UserDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}

		return resultList;
	}

}
