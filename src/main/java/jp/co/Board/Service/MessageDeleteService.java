package jp.co.Board.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.form.MessageDeleteForm;
import jp.co.Board.mapper.MessageMapper;

@Service
public class MessageDeleteService {

	@Autowired
	private MessageMapper messageMapper;

	public int delete(MessageDeleteForm deleteForm) {
		int count = messageMapper.delete(deleteForm);
		return count;
	}

}
