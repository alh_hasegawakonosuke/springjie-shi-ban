package jp.co.Board.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.dto.UserDto;
import jp.co.Board.entity.UserEntity;
import jp.co.Board.form.UserEditForm;
import jp.co.Board.mapper.UserMapper;

@Service
public class UserEditService {

	@Autowired
	private UserMapper userMapper;

	public UserDto getUser(int id) {
		UserDto dto = new UserDto();
		UserEntity entity = userMapper.getEditUser(id);
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

	public int edit(UserEditForm userEditForm) {
		int count = userMapper.edit(userEditForm);
		return count;
	}

}
