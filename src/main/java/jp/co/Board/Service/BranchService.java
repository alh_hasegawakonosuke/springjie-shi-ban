package jp.co.Board.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.Board.dto.BranchDto;
import jp.co.Board.entity.BranchEntity;
import jp.co.Board.mapper.BranchMapper;

@Service
public class BranchService {

	@Autowired
	private BranchMapper branchMapper;

	public List<BranchDto> getBranch(){
		List<BranchEntity> branchList = branchMapper.getBranch();
		List<BranchDto> resultList = convertToDto(branchList);
		return resultList;

	}

	private List<BranchDto> convertToDto(List<BranchEntity> branchList){
		List<BranchDto> resultList = new LinkedList<>();
		for(BranchEntity entity : branchList) {
			BranchDto dto = new BranchDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}

		return resultList;
	}

}
